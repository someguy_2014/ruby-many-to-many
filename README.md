# ruby-many-to-many

Many to many relationship using only two tables written in ruby


```
class User < ActiveRecord::Base
  has_many :user_followers, foreign_key: :followee_id, class_name: 'UserFollowers' 
  has_many :followers, through: :user_followers, source: :follower

  has_many :user_followees, foreign_key: :follower_id, class_name: 'UserFollowers'    
  has_many :followees, through: :user_followees, source: :followee
end

class UserFollower < ActiveRecord::Base
  belongs_to :follower, foreign_key: :follower_id, class_name: 'User'
  belongs_to :followee, foreign_key: :followee_id, class_name: 'User'
end
```
